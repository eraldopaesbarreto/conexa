FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY ./target/conexa-0.0.1-SNAPSHOT.jar conexa.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom","-jar","/conexa.jar", "--spring.profiles.active=${SPRING_PROFILES_ACTIVE}"]