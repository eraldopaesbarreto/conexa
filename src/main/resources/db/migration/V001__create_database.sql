CREATE TABLE specialties
(
    specialty_id   INT(8)      NOT NULL AUTO_INCREMENT,
    specialty_name VARCHAR(20) NOT NULL,
    PRIMARY KEY (specialty_id)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8;

CREATE TABLE doctors
(
    doctor_id           INT(8)       NOT NULL AUTO_INCREMENT,
    doctor_name         VARCHAR(50)  NOT NULL,
    doctor_email        VARCHAR(50)  NOT NULL,
    doctor_password     VARCHAR(200) NOT NULL,
    doctor_specialty_id INT(8)       NOT NULL,
    PRIMARY KEY (doctor_id),
    UNIQUE KEY email_UNIQUE (doctor_email),
    CONSTRAINT doctor_specialty_id_fk FOREIGN KEY (doctor_specialty_id) REFERENCES specialties (specialty_id)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8;

CREATE TABLE patients
(
    patient_id    INT(8)      NOT NULL AUTO_INCREMENT,
    patient_name  VARCHAR(50) NOT NULL,
    patient_cpf   VARCHAR(11) NOT NULL,
    patient_age   INT(8)      NOT NULL,
    patient_phone VARCHAR(11) NOT NULL,
    PRIMARY KEY (patient_id),
    UNIQUE KEY cpf_UNIQUE (patient_cpf)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8;

CREATE TABLE appointments
(
    appointment_id         INT(8)   NOT NULL AUTO_INCREMENT,
    appointment_doctor_id  INT(8)   NOT NULL,
    appointment_patient_id INT(8)   NOT NULL,
    appointment_date       DATETIME NOT NULL,
    PRIMARY KEY (appointment_id),
    CONSTRAINT appointment_doctor_id_fk FOREIGN KEY (appointment_doctor_id) REFERENCES doctors (doctor_id)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    CONSTRAINT appointment_patient_id_fk FOREIGN KEY (appointment_patient_id) REFERENCES patients (patient_id)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8;

INSERT INTO specialties
VALUES (1, 'Cardiologista'),
       (2, 'Oftalmologista'),
       (3, 'Pediatra');

INSERT INTO doctors
VALUES (1, 'medico1', 'medico1@conexa.com.br', '$2a$10$Zs9Aj9R0N3/9ehiFRQL3WejSstdO/0eDYBkKw0Sam2RkOZc6D.6vy', 1),
       (2, 'medico2', 'medico2@conexa.com.br', '$2a$10$Zs9Aj9R0N3/9ehiFRQL3WejSstdO/0eDYBkKw0Sam2RkOZc6D.6vy', 2),
       (3, 'medico3', 'medico3@conexa.com.br', '$2a$10$Zs9Aj9R0N3/9ehiFRQL3WejSstdO/0eDYBkKw0Sam2RkOZc6D.6vy', 3);

INSERT INTO patients
VALUES (1, 'João Paulo', '05586115085', 35, '79991728571');
INSERT INTO patients
VALUES (2, 'Maria Barreto', '71342018052', 40, '79891728570');
INSERT INTO patients
VALUES (3, 'José Maria', '64256937030', 70, '79991728470');
INSERT INTO patients
VALUES (4, 'Maísa Figueredo', '47474588080', 19, '79991748570');

INSERT INTO appointments
VALUES (1, 1, 1, '2020-07-15 15:00:00'),
       (2, 1, 2, '2020-07-16 16:00:00');
