package br.com.conexa.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TokenDTO {

    @NotEmpty(message = "token-dto.token-required")
    private String token;
}
