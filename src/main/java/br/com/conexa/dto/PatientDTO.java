package br.com.conexa.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PatientDTO {

    private Integer id;

    @NotEmpty(message = "patient-dto.name-required")
    @Size(max = 50, message = "patient-dto.incorrect-size")
    private String name;

    @CPF(message = "patient-dto.invalid-cpf")
    private String cpf;

    @NotNull(message = "patient-dto.age-required")
    private Integer age;

    @Pattern(regexp = "[0-9]{11}", message = "patient-dto.invalid-phone")
    private String phone;
}
