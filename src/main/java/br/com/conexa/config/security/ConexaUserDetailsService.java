package br.com.conexa.config.security;

import br.com.conexa.domain.Doctor;
import br.com.conexa.domain.DoctorUser;
import br.com.conexa.service.DoctorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;

@Service
public class ConexaUserDetailsService implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConexaUserDetailsService.class);

    @Autowired
    private DoctorService doctorService;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        Optional<Doctor> doctor = doctorService.findByEmail(email);

        if (!doctor.isPresent()) {
            LOGGER.error("Usuário com e-mail {} não existe!", email);
            throw new UsernameNotFoundException("Usuário e/ou senha inválido(s)");
        }

        return new DoctorUser(doctor.get(), new HashSet<>());
    }
}
