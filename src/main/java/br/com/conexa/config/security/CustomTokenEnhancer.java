package br.com.conexa.config.security;

import br.com.conexa.domain.Appointment;
import br.com.conexa.domain.DoctorUser;
import br.com.conexa.dto.AppointmentDTO;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class CustomTokenEnhancer implements TokenEnhancer {

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {

        DoctorUser doctorUser = (DoctorUser) authentication.getPrincipal();

        HashMap<String, Object> addInfo = new HashMap<>();
        addInfo.put("doctor_email", doctorUser.getDoctor().getEmail());
        addInfo.put("doctor_name", doctorUser.getDoctor().getName());
        addInfo.put("doctor_specialty", doctorUser.getDoctor().getSpecialty().getName());
        addInfo.put("doctor_today_appointments", getAppointments(doctorUser.getDoctor().getAppointments()));
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(addInfo);

        return accessToken;
    }

    private List<AppointmentDTO> getAppointments(List<Appointment> appointments) {
        return appointments.stream()
                .filter(appointment -> appointment.getDate().toLocalDate().equals(LocalDate.now()))
                .map(appointment -> new AppointmentDTO(appointment.getPatient().getId(), appointment.getDate()))
                .collect(Collectors.toList());
    }
}
