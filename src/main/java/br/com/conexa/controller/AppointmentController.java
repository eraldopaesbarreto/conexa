package br.com.conexa.controller;

import br.com.conexa.dto.AppointmentDTO;
import br.com.conexa.service.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/appointments")
public class AppointmentController {

    private final AppointmentService appointmentService;

    @Autowired
    public AppointmentController(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    @PostMapping
    public ResponseEntity<Void> save(@Valid @RequestBody AppointmentDTO appointmentDTO, HttpServletRequest request) {
        appointmentService.save(appointmentDTO, request);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
