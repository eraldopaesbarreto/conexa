package br.com.conexa.controller;

import br.com.conexa.dto.PatientDTO;
import br.com.conexa.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/patients")
public class PatientController {

    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping
    public ResponseEntity<List<PatientDTO>> findAll() {
        List<PatientDTO> patientDTOList = patientService.findAll();
        return ResponseEntity.ok(patientDTOList);
    }

    @PostMapping
    public ResponseEntity<PatientDTO> save(@Valid @RequestBody PatientDTO patientDTO) {
        PatientDTO saved = patientService.save(patientDTO);
        return new ResponseEntity<>(saved, HttpStatus.CREATED);
    }

    @PutMapping("/{patientId}")
    public ResponseEntity<PatientDTO> update(@Valid @RequestBody PatientDTO patientDTO, @PathVariable Integer patientId) {
        PatientDTO updatedPatientDTO = patientService.update(patientDTO, patientId);
        return ResponseEntity.ok(updatedPatientDTO);
    }

    @DeleteMapping("/{patientId}")
    public ResponseEntity<Void> delete(@PathVariable Integer patientId) {
        patientService.delete(patientId);
        return ResponseEntity.noContent().build();
    }
}
