package br.com.conexa.service;

import br.com.conexa.exceptionhandler.BusinessException;
import br.com.conexa.exceptionhandler.SecurityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.nonNull;

@Service
public class TokenManagementService {

    private final RedisTemplate<String, String> redisTemplate;

    @Autowired
    public TokenManagementService(RedisTemplate<String, String> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public void invalidateToken(HttpServletRequest request) {
        String token = extractToken(request);
        redisTemplate.opsForValue().set(token, token);
        redisTemplate.expire(token, 24, TimeUnit.HOURS);
    }

    public void verifyKey(HttpServletRequest request) {
        String token = extractToken(request);
        Boolean hasKey = redisTemplate.hasKey(token);
        if (nonNull(hasKey) && hasKey)
            throw new SecurityException("token-management-service.invalid-token");
    }

    // Método Retirado da classe BearerTokenExtractor
    public String extractToken(HttpServletRequest request) {
        Enumeration headers = request.getHeaders("Authorization");

        String value;
        do {
            if (!headers.hasMoreElements()) {
                return null;
            }
            value = (String)headers.nextElement();
        } while(!value.toLowerCase().startsWith("Bearer".toLowerCase()));

        String authHeaderValue = value.substring("Bearer".length()).trim();
        request.setAttribute(OAuth2AuthenticationDetails.ACCESS_TOKEN_TYPE, value.substring(0, "Bearer".length()).trim());
        int commaIndex = authHeaderValue.indexOf(44);
        if (commaIndex > 0) {
            authHeaderValue = authHeaderValue.substring(0, commaIndex);
        }

        return authHeaderValue;
    }
}
