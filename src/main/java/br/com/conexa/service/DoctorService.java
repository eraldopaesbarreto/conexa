package br.com.conexa.service;

import br.com.conexa.domain.Doctor;
import br.com.conexa.exceptionhandler.BusinessException;
import br.com.conexa.repository.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Service
public class DoctorService {

    private final DoctorRepository doctorRepository;
    private final TokenManagementService tokenManagementService;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository,
                         TokenManagementService tokenManagementService) {
        this.doctorRepository = doctorRepository;
        this.tokenManagementService = tokenManagementService;

    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Optional<Doctor> findByEmail(String email) {
        return doctorRepository.findByEmail(email);
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Doctor findByEmail() {
        String email = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return doctorRepository.findByEmail(email).orElseThrow(() -> new BusinessException("doctor-service.not-found"));
    }

    public void logout(HttpServletRequest request) {
        tokenManagementService.invalidateToken(request);
    }
}
