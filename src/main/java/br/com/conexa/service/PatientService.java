package br.com.conexa.service;

import br.com.conexa.domain.Patient;
import br.com.conexa.dto.PatientDTO;
import br.com.conexa.exceptionhandler.BusinessException;
import br.com.conexa.mapper.PatientMapper;
import br.com.conexa.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PatientService {

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private PatientMapper patientMapper;

    @Autowired
    private AppointmentService appointmentService;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<PatientDTO> findAll() {
        List<Patient> patients = patientRepository.findAll();
        return patientMapper.toDTO(patients);
    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Patient findById(Integer patientId) {
        return patientRepository.findById(patientId)
                .orElseThrow(() -> new BusinessException("patient-service.not-found"));
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public PatientDTO save(PatientDTO patientDTO) {
        Optional<Patient> byCpf = patientRepository.findByCpf(patientDTO.getCpf());
        if (byCpf.isPresent())
            throw new BusinessException("patient-service.already-exist");
        Patient patient = patientMapper.toEntity(patientDTO);
        Patient savedPatient = patientRepository.save(patient);
        return patientMapper.toDTO(savedPatient);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public PatientDTO update(PatientDTO patientDTO, Integer patientId) {
        Patient patient = patientRepository.findById(patientId)
                .orElseThrow(() -> new BusinessException("patient-service.not-found"));

        Optional<Patient> byCpf = patientRepository.findByCpfAndIdIsNot(patientDTO.getCpf(), patientId);
        if (byCpf.isPresent())
            throw new BusinessException("patient-service.already-exist");

        patient.setName(patientDTO.getName());
        patient.setAge(patientDTO.getAge());
        patient.setCpf(patientDTO.getCpf());
        patient.setPhone(patientDTO.getPhone());
        Patient updatedPatient = patientRepository.save(patient);
        return patientMapper.toDTO(updatedPatient);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void delete(Integer patientId) {
        Patient patient = patientRepository.findById(patientId)
                .orElseThrow(() -> new BusinessException("patient-service.not-found"));
        appointmentService.deleteByPatient(patient);
        patientRepository.delete(patient);
    }
}
