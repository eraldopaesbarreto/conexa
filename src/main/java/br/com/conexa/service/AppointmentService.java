package br.com.conexa.service;

import br.com.conexa.domain.Appointment;
import br.com.conexa.domain.Doctor;
import br.com.conexa.domain.Patient;
import br.com.conexa.dto.AppointmentDTO;
import br.com.conexa.repository.AppointmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;

@Service
public class AppointmentService {

    @Autowired
    private AppointmentRepository appointmentRepository;
    @Autowired
    private PatientService patientService;
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private TokenManagementService tokenManagementService;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void save(AppointmentDTO appointmentDTO, HttpServletRequest request) {
        tokenManagementService.verifyKey(request);

        Patient patient = patientService.findById(appointmentDTO.getPatientId());
        Doctor doctor = doctorService.findByEmail();

        Appointment appointment = new Appointment();
        appointment.setDoctor(doctor);
        appointment.setPatient(patient);
        appointment.setDate(appointmentDTO.getDate());

        appointmentRepository.save(appointment);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void deleteByPatient(Patient patient) {
        appointmentRepository.deleteAllByPatient(patient);
    }
}
