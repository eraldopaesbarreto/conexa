package br.com.conexa.repository;

import br.com.conexa.domain.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Integer> {

    Optional<Patient> findByCpf(String cpf);

    Optional<Patient> findByCpfAndIdIsNot(String cpf, Integer patientId);
}
