package br.com.conexa.repository;

import br.com.conexa.domain.Appointment;
import br.com.conexa.domain.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Integer> {

    void deleteAllByPatient(Patient patient);
}
