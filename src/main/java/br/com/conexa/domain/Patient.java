package br.com.conexa.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "patients")
public class Patient implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "patient_id")
    private Integer id;

    @Column(name = "patient_name")
    private String name;

    @Column(name = "patient_cpf")
    private String cpf;

    @Column(name = "patient_age")
    private Integer age;

    @Column(name = "patient_phone")
    private String phone;
}
