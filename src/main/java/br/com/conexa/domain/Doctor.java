package br.com.conexa.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@ToString(exclude = "appointments")
@EqualsAndHashCode(of = "id")
@Table(name = "doctors")
public class Doctor implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "doctor_id")
    private Integer id;

    @Column(name = "doctor_name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "doctor_specialty_id", referencedColumnName = "specialty_id")
    private Specialty specialty;

    @Column(name = "doctor_email")
    private String email;

    @Column(name = "doctor_password")
    private String password;

    @OneToMany(mappedBy = "doctor", fetch = FetchType.EAGER)
    private List<Appointment> appointments = new ArrayList<>();

}
