package br.com.conexa.domain;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class DoctorUser extends User {

    private final Doctor doctor;

    public DoctorUser(Doctor doctor, Collection<? extends GrantedAuthority> authorities) {
        super(doctor.getEmail(), doctor.getPassword(), authorities);
        this.doctor = doctor;
    }

    public Doctor getDoctor() {
        return doctor;
    }
}
