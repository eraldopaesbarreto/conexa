package br.com.conexa.exceptionhandler;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class BusinessException extends RuntimeException {

    private final HttpStatus status;
    private final String errorCode;

    public BusinessException(String errorCode) {
        this.status = HttpStatus.BAD_REQUEST;
        this.errorCode = errorCode;
    }
}
