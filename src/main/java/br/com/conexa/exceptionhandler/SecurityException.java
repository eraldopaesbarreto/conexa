package br.com.conexa.exceptionhandler;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class SecurityException extends RuntimeException {

    private final HttpStatus status;
    private final String errorCode;

    public SecurityException(String errorCode) {
        this.status = HttpStatus.UNAUTHORIZED;
        this.errorCode = errorCode;
    }
}
