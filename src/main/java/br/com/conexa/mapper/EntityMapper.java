package br.com.conexa.mapper;

import java.util.List;

public interface EntityMapper<E, D> {

    E toEntity(D d);

    D toDTO(E e);

    List<E> toEntity(List<D> d);

    List<D> toDTO(List<E> e);

}
