package br.com.conexa.mapper;

import br.com.conexa.domain.Patient;
import br.com.conexa.dto.PatientDTO;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PatientMapper implements EntityMapper<Patient, PatientDTO> {

    public Patient toEntity(PatientDTO patientDTO) {
        Patient patient = new Patient();
        patient.setId(patientDTO.getId());
        patient.setName(patientDTO.getName());
        patient.setAge(patientDTO.getAge());
        patient.setCpf(patientDTO.getCpf());
        patient.setPhone(patientDTO.getPhone());
        return patient;
    }

    public PatientDTO toDTO(Patient patient) {
        PatientDTO patientDTO = new PatientDTO();
        patientDTO.setId(patient.getId());
        patientDTO.setName(patient.getName());
        patientDTO.setAge(patient.getAge());
        patientDTO.setCpf(patient.getCpf());
        patientDTO.setPhone(patient.getPhone());
        return patientDTO;
    }

    public List<PatientDTO> toDTO(List<Patient> patients) {
        return patients
                .stream()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }

    public List<Patient> toEntity(List<PatientDTO> patientDTOS) {
        return patientDTOS
                .stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }
}
