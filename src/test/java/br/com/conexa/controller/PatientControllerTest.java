package br.com.conexa.controller;

import br.com.conexa.MockitoExtension;
import br.com.conexa.dto.PatientDTO;
import br.com.conexa.service.PatientService;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PatientControllerTest {

    @InjectMocks
    private PatientController patientController;

    @Mock
    private PatientService patientService;

    @Test
    @DisplayName("Deve retornar todos os pacientes")
    public void should_return_all_patients() {

        when(patientService.findAll()).thenReturn(createResponse());

        PatientDTO[] response = given()
                .standaloneSetup(patientController)
                .log().all()
                .when()
                .get("/patients")
                .then()
                .statusCode(HttpStatus.OK.value())
                .extract()
                .as(PatientDTO[].class);

        verify(patientService, times(1)).findAll();
        assertEquals(2, response.length);
    }

    @Test
    @DisplayName("Deve salvar os dados do paciente")
    public void should_save_patient() {

        PatientDTO patientDTO = createPatientDTO(null, "Test", 30, "71644721066", "00000000000");
        PatientDTO savedPatientDTO = createPatientDTO(1, "Test", 30, "71644721066", "00000000000");

        when(patientService.save(patientDTO)).thenReturn(savedPatientDTO);

        PatientDTO response = given()
                .standaloneSetup(patientController)
                .contentType(ContentType.JSON)
                .body(patientDTO)
                .log().all()
                .when()
                .post("/patients")
                .then()
                .statusCode(HttpStatus.CREATED.value())
                .extract()
                .as(PatientDTO.class);

        verify(patientService, times(1)).save(patientDTO);
        assertEquals(Integer.valueOf(1), response.getId());
    }

    @Test
    @DisplayName("Deve atualizar os dados do paciente")
    public void should_update_patient() {

        PatientDTO patientDTO = createPatientDTO(1, "Test", 30, "71644721066", "00000000000");
        PatientDTO updatedPatientDTO = createPatientDTO(1, "Test", 30, "71644721066", "00000000000");

        when(patientService.update(patientDTO,1)).thenReturn(updatedPatientDTO);

        PatientDTO response = given()
                .standaloneSetup(patientController)
                .contentType(ContentType.JSON)
                .body(patientDTO)
                .log().all()
                .when()
                .put("/patients/{patientsId}", 1)
                .then()
                .statusCode(HttpStatus.OK.value())
                .extract()
                .as(PatientDTO.class);

        verify(patientService, times(1)).update(patientDTO, 1);
        assertEquals(Integer.valueOf(1), response.getId());
    }

    @Test
    @DisplayName("Deve deletar os dados do paciente")
    public void should_delete_patient() {

        doNothing().when(patientService).delete(1);

        given()
                .standaloneSetup(patientController)
                .delete("/patients/{patientsId}", 1)
                .then()
                .statusCode(HttpStatus.NO_CONTENT.value());

        verify(patientService, times(1)).delete(1);
    }

    @ParameterizedTest
    @MethodSource("providePatientsDTOS")
    @DisplayName("Deve retornar status 400 devido erro de validações ao salvar paciente")
    public void should_handle_validations_errors(String name,
                                                 Integer age,
                                                 String cpf,
                                                 String phone) {

        PatientDTO patientDTO = createPatientDTO(null, name, age, cpf, phone);

        given()
                .standaloneSetup(patientController)
                .contentType(ContentType.JSON)
                .body(patientDTO)
                .log().all()
                .when()
                .post("/patients")
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());

        verify(patientService, never()).save(any());
    }

    private static Stream<Arguments> providePatientsDTOS() {
        return Stream.of(
                Arguments.of("", 30, "71644721066", "00000000000"),
                Arguments.of("TestTestTestTestTestTestTestTestTestTestTestTestTest", 30, "71644721066", "00000000000"),
                Arguments.of("Test", 30, "71641721066", "00000000000"),
                Arguments.of("Test", null, "71644721066", "00000000000"),
                Arguments.of("Test", 30, "71644721066", "0000000000")
        );
    }

    private List<PatientDTO> createResponse() {

        PatientDTO patient1 = new PatientDTO();
        PatientDTO patient2 = new PatientDTO();

        return Arrays.asList(patient1, patient2);
    }

    private PatientDTO createPatientDTO(Integer id, String name, Integer age, String cpf, String phone) {
        PatientDTO patientDTO = new PatientDTO();
        patientDTO.setId(id);
        patientDTO.setName(name);
        patientDTO.setAge(age);
        patientDTO.setCpf(cpf);
        patientDTO.setPhone(phone);
        return patientDTO;
    }
}