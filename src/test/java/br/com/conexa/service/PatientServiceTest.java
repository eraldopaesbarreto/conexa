package br.com.conexa.service;

import br.com.conexa.MockitoExtension;
import br.com.conexa.domain.Patient;
import br.com.conexa.dto.PatientDTO;
import br.com.conexa.exceptionhandler.BusinessException;
import br.com.conexa.mapper.PatientMapper;
import br.com.conexa.repository.PatientRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class PatientServiceTest {

    @InjectMocks
    private PatientService patientService;

    @Mock
    private PatientRepository patientRepository;

    @Mock
    private PatientMapper patientMapper;

    @Mock
    private AppointmentService appointmentService;

    @Test
    @DisplayName("Deve retornar todos os pacientes")
    void should_return_all_patients() {

        Patient patient1 = mock(Patient.class);
        Patient patient2 = mock(Patient.class);

        PatientDTO patientDTO1 = mock(PatientDTO.class);
        PatientDTO patientDTO2 = mock(PatientDTO.class);

        List<Patient> patients = new ArrayList<>();
        patients.add(patient1);
        patients.add(patient2);

        List<PatientDTO> patientDTOS = new ArrayList<>();
        patientDTOS.add(patientDTO1);
        patientDTOS.add(patientDTO2);

        when(patientRepository.findAll()).thenReturn(patients);
        when(patientMapper.toDTO(patients)).thenReturn(patientDTOS);

        List<PatientDTO> response = patientService.findAll();

        assertEquals(2, response.size());
        verify(patientRepository, times(1)).findAll();
        verify(patientMapper, times(1)).toDTO(patients);
    }

    @Nested
    @DisplayName("Deve buscar um paciente pelo id")
    class PatientById {

        @Test
        @DisplayName("Deve retornar um paciente pesquisando id")
        void should_return_patient_by_id_with_success() {

            Patient patient = new Patient();
            patient.setId(1);
            patient.setName("Test");

            when(patientRepository.findById(1)).thenReturn(Optional.of(patient));

            Patient response = patientService.findById(1);

            assertEquals(response.getId(), patient.getId());

            verify(patientRepository, times(1)).findById(1);
        }

        @Test
        @DisplayName("Deve lançar erro ao buscar paciente por id")
        void should_throw_exception() {

            when(patientRepository.findById(1)).thenReturn(Optional.empty());
            BusinessException businessException = assertThrows(BusinessException.class,
                    () -> patientService.findById(1));

            verify(patientRepository, times(1)).findById(1);

            assertEquals("patient-service.not-found", businessException.getErrorCode());
            assertEquals(HttpStatus.BAD_REQUEST, businessException.getStatus());
        }
    }

    @Nested
    @DisplayName("Deve salvar um paciente")
    class PatientSave {

        @Test
        @DisplayName("Deve salvar um paciente com sucesso")
        void should_save_patient_with_success() {

            Patient patient = new Patient();
            patient.setName("Test");
            patient.setAge(40);
            patient.setPhone("79991728652");
            patient.setCpf("11691759023");

            PatientDTO patientDTO = new PatientDTO();
            patientDTO.setName("Test");
            patientDTO.setAge(40);
            patientDTO.setPhone("79991728652");
            patientDTO.setCpf("11691759023");

            Patient savedPatient = new Patient();
            savedPatient.setId(1);
            savedPatient.setName("Test");
            savedPatient.setAge(40);
            savedPatient.setPhone("79991728652");
            savedPatient.setCpf("11691759023");

            PatientDTO patientDTOReturn = new PatientDTO();
            patientDTOReturn.setId(1);
            patientDTOReturn.setName("Test");
            patientDTOReturn.setAge(40);
            patientDTOReturn.setPhone("79991728652");
            patientDTOReturn.setCpf("11691759023");

            when(patientRepository.findByCpf("11691759023")).thenReturn(Optional.empty());
            when(patientMapper.toEntity(patientDTO)).thenReturn(patient);
            when(patientRepository.save(patient)).thenReturn(savedPatient);
            when(patientMapper.toDTO(savedPatient)).thenReturn(patientDTOReturn);

            PatientDTO response = patientService.save(patientDTO);

            verify(patientRepository, times(1)).findByCpf(anyString());
            verify(patientMapper, times(1)).toEntity(any(PatientDTO.class));
            verify(patientRepository, times(1)).save(any(Patient.class));
            verify(patientMapper, times(1)).toDTO(any(Patient.class));

            assertEquals(response.getId(), patientDTOReturn.getId());
            assertEquals(response.getCpf(), patientDTOReturn.getCpf());
            assertEquals(response.getAge(), patientDTOReturn.getAge());
            assertEquals(response.getPhone(), patientDTOReturn.getPhone());
            assertEquals(response.getName(), patientDTOReturn.getName());
        }

        @Test
        @DisplayName("Deve lançar erro ao cadastrar paciente com cpf já existente")
        void should_throw_exception() {

            Patient patient = new Patient();
            patient.setId(1);
            patient.setName("Test");
            patient.setAge(40);
            patient.setPhone("79991728652");
            patient.setCpf("11691759023");

            PatientDTO patientDTO = new PatientDTO();
            patientDTO.setName("Test");
            patientDTO.setAge(40);
            patientDTO.setPhone("79991728652");
            patientDTO.setCpf("11691759023");

            when(patientRepository.findByCpf("11691759023")).thenReturn(Optional.of(patient));
            BusinessException businessException = assertThrows(BusinessException.class,
                    () -> patientService.save(patientDTO));

            verify(patientRepository, times(1)).findByCpf(anyString());
            verify(patientMapper, never()).toEntity(any(PatientDTO.class));
            verify(patientRepository, never()).save(any(Patient.class));
            verify(patientMapper, never()).toDTO(any(Patient.class));

            assertEquals("patient-service.already-exist", businessException.getErrorCode());
            assertEquals(HttpStatus.BAD_REQUEST, businessException.getStatus());
        }
    }

    @Nested
    @DisplayName("Deve atualizar um paciente")
    class PatientUpdate {

        @Test
        @DisplayName("Deve atualizar um paciente com sucesso")
        void should_update_patient_with_success() {

            Patient patient = new Patient();
            patient.setId(1);
            patient.setName("Test");
            patient.setAge(40);
            patient.setPhone("79991728652");
            patient.setCpf("11691759023");

            PatientDTO patientDTO = new PatientDTO();
            patientDTO.setName("Test1");
            patientDTO.setAge(45);
            patientDTO.setPhone("79991728654");
            patientDTO.setCpf("11691759023");

            Patient updatedPatient = new Patient();
            updatedPatient.setId(1);
            updatedPatient.setName("Test1");
            updatedPatient.setAge(45);
            updatedPatient.setPhone("79991728654");
            updatedPatient.setCpf("11691759023");

            PatientDTO patientDTOReturn = new PatientDTO();
            patientDTOReturn.setId(1);
            patientDTOReturn.setName("Test1");
            patientDTOReturn.setAge(45);
            patientDTOReturn.setPhone("79991728654");
            patientDTOReturn.setCpf("11691759023");

            when(patientRepository.findById(1)).thenReturn(Optional.of(patient));
            when(patientRepository.findByCpfAndIdIsNot("11691759023", 1)).thenReturn(Optional.empty());
            when(patientRepository.save(patient)).thenReturn(updatedPatient);
            when(patientMapper.toDTO(updatedPatient)).thenReturn(patientDTOReturn);

            PatientDTO response = patientService.update(patientDTO, 1);

            verify(patientRepository, times(1)).findById(anyInt());
            verify(patientRepository, times(1)).findByCpfAndIdIsNot(anyString(), anyInt());
            verify(patientRepository, times(1)).save(any(Patient.class));
            verify(patientMapper, times(1)).toDTO(any(Patient.class));

            assertEquals(response.getId(), patientDTOReturn.getId());
            assertEquals(response.getCpf(), patientDTOReturn.getCpf());
            assertEquals(response.getAge(), patientDTOReturn.getAge());
            assertEquals(response.getPhone(), patientDTOReturn.getPhone());
            assertEquals(response.getName(), patientDTOReturn.getName());
        }

        @Test
        @DisplayName("Deve lançar erro ao tentar atualizar um paciente que não existe")
        void should_throw_exception_not_found() {

            PatientDTO patientDTO = new PatientDTO();
            patientDTO.setName("Test1");
            patientDTO.setAge(45);
            patientDTO.setPhone("79991728654");
            patientDTO.setCpf("11691759023");

            when(patientRepository.findById(1)).thenReturn(Optional.empty());
            BusinessException businessException = assertThrows(BusinessException.class,
                    () -> patientService.update(patientDTO, 1));

            verify(patientRepository, times(1)).findById(anyInt());
            verify(patientRepository, never()).findByCpfAndIdIsNot(anyString(), anyInt());
            verify(patientRepository, never()).save(any(Patient.class));
            verify(patientMapper, never()).toDTO(any(Patient.class));

            assertEquals("patient-service.not-found", businessException.getErrorCode());
            assertEquals(HttpStatus.BAD_REQUEST, businessException.getStatus());
        }

        @Test
        @DisplayName("Deve lançar erro ao tentar atualizar um paciente com cpf já existente")
        void should_throw_exception_cpf_already_exists() {

            Patient patient = new Patient();
            patient.setId(1);
            patient.setName("Test");
            patient.setAge(40);
            patient.setPhone("79991728652");
            patient.setCpf("11691759023");

            Patient patient2 = new Patient();
            patient.setId(2);
            patient.setName("Test1");
            patient.setAge(24);
            patient.setPhone("79991428652");
            patient.setCpf("49817615030");

            PatientDTO patientDTO = new PatientDTO();
            patientDTO.setName("Test1");
            patientDTO.setAge(45);
            patientDTO.setPhone("79991728654");
            patientDTO.setCpf("49817615030");

            when(patientRepository.findById(1)).thenReturn(Optional.of(patient));
            when(patientRepository.findByCpfAndIdIsNot("49817615030", 1)).thenReturn(Optional.of(patient2));
            BusinessException businessException = assertThrows(BusinessException.class,
                    () -> patientService.update(patientDTO, 1));

            verify(patientRepository, times(1)).findById(anyInt());
            verify(patientRepository, times(1)).findByCpfAndIdIsNot(anyString(), anyInt());
            verify(patientRepository, never()).save(any(Patient.class));
            verify(patientMapper, never()).toDTO(any(Patient.class));

            assertEquals("patient-service.already-exist", businessException.getErrorCode());
            assertEquals(HttpStatus.BAD_REQUEST, businessException.getStatus());
        }
    }

    @Nested
    @DisplayName("Deve deletar um paciente")
    class PatientDelete {

        @Test
        @DisplayName("Deve deletar um paciente com sucesso")
        void should_delete_patient_with_success() {

            Patient patient = new Patient();
            patient.setId(1);

            when(patientRepository.findById(1)).thenReturn(Optional.of(patient));
            doNothing().when(appointmentService).deleteByPatient(patient);
            doNothing().when(patientRepository).delete(patient);

            patientService.delete(1);

            verify(patientRepository, times(1)).findById(anyInt());
            verify(appointmentService, times(1)).deleteByPatient(any(Patient.class));
            verify(patientRepository, times(1)).delete(any(Patient.class));
        }

        @Test
        @DisplayName("Deve lançar erro ao tentar deletar um paciente que não existe")
        void should_throw_exception_not_found() {

            when(patientRepository.findById(1)).thenReturn(Optional.empty());
            BusinessException businessException = assertThrows(BusinessException.class,
                    () -> patientService.delete(1));

            verify(patientRepository, times(1)).findById(anyInt());
            verify(appointmentService, never()).deleteByPatient(any(Patient.class));
            verify(patientRepository, never()).delete(any(Patient.class));

            assertEquals("patient-service.not-found", businessException.getErrorCode());
            assertEquals(HttpStatus.BAD_REQUEST, businessException.getStatus());
        }
    }
}