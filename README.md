Para rodar o projeto:

 - Docker instalado
 - Na pasta docker do projeto tem um compose para subir redis e mysql
 - ./mvnw spring-boot:run para rodar de fato o projeto
 - para rodar os testes faça: mvn test
 
A API foi disponibilizada em um cluster kubernetes para apreciação mais rápida, bastando para isso utilizar os recursos da pasta postman do projeto